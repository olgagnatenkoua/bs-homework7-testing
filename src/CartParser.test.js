import CartParser from "./CartParser";
import * as fs from "fs";
import { v4 } from "uuid";
const nodePath = require("path");
const PRECISION = 5;

jest.mock("fs");
jest.mock("uuid/v4", () => () => "1");

let parser;
let parse, readFile, validate, parseLine, calcTotal, createError;
beforeEach(() => {
  parser = new CartParser();
  parse = parser.parse.bind(parser);
  readFile = parser.readFile.bind(parser);
  validate = parser.validate.bind(parser);
  parseLine = parser.parseLine.bind(parser);
  calcTotal = parser.calcTotal.bind(parser);
  createError = parser.createError.bind(parser);
  jest.resetAllMocks();
});

describe("CartParser class structure", () => {
  it("should contain expected attributes ColumnType, ErrorType, schema", () => {
    expect(parser.ColumnType).toEqual({
      STRING: "string",
      NUMBER_POSITIVE: "numberPositive"
    });
    expect(parser.ErrorType).toEqual({
      HEADER: "header",
      ROW: "row",
      CELL: "cell"
    });
    expect(parser.schema).toEqual({
      columns: [
        {
          name: "Product name",
          key: "name",
          type: "string"
        },
        {
          name: "Price",
          key: "price",
          type: "numberPositive"
        },
        {
          name: "Quantity",
          key: "quantity",
          type: "numberPositive"
        }
      ]
    });
  });
  it("should contain correct class methods", () => {
    const methodNames = [
      "parse",
      "readFile",
      "validate",
      "parseLine",
      "calcTotal",
      "createError"
    ];
    methodNames.forEach(method => {
      expect(typeof parser[method]).toEqual("function");
    });
  });
});

describe("readFile", () => {
  it("should call readFileSync with path to file, utf-8 encoding, read access", () => {
    const pathToCsv = nodePath.join(__dirname, "/../samples/cart.csv");
    const spy = jest.spyOn(fs, "readFileSync");
    readFile(pathToCsv);
    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith(pathToCsv, "utf-8", "r");
  });
});

describe("createError", () => {
  it("should create Error object with type, row, column, message", () => {
    const type = "Test Type";
    const row = 1;
    const column = 3;
    const message = "Error in row 1, column 3";
    const error = createError(type, row, column, message);
    expect(error).toEqual({ type, row, column, message });
  });
});

describe("calcTotal", () => {
  it("should calculate correct Total for an array of items", () => {
    const cartItems = [
      {
        id: "3e6def17-5e87-4f27-b6b8-ae78948523a9",
        name: "Mollis consequat",
        price: 9,
        quantity: 2
      },
      {
        id: "90cd22aa-8bcf-4510-a18d-ec14656d1f6a",
        name: "Tvoluptatem",
        price: 9.3,
        quantity: 1
      }
    ];
    expect(calcTotal(cartItems)).toBeCloseTo(27.3, PRECISION);
  });
  it("should calculate 0 Total for an empty array of items", () => {
    expect(calcTotal([])).toBe(0);
  });
});

// parseLine assumes that csv line is already valid; validation is tested in separate unit test
describe("parseLine", () => {
  it("should generate correct Item object from valid csvLine", () => {
    const csvLine = "Mollis consequat,9,2";
    const item = parseLine(csvLine);
    expect(item).toEqual({
      id: "1",
      name: "Mollis consequat",
      price: 9,
      quantity: 2
    });
  });
  it("should trim extra spaces from csvLine items", () => {
    const csvLine = " Mollis consequat , 9  ,  2  ";
    const item = parseLine(csvLine);
    expect(item).toEqual({
      id: "1",
      name: "Mollis consequat",
      price: 9,
      quantity: 2
    });
  });
});

describe("validate - headers", () => {
  const contents = "Product name,Price,Quantity";
  it("should return no errors for correct header", () => {
    const errors = validate(contents);
    expect(errors).toEqual([]);
  });
  it("should return error for header with incorrect header name", () => {
    const headerArray = contents.split(",");
    let incorrectHeaderArray = [];
    let errors = [];
    headerArray.forEach((item, idx) => {
      incorrectHeaderArray = [...headerArray];
      incorrectHeaderArray.splice(idx, 1, "1");
      errors = validate(incorrectHeaderArray.join(","));
      expect(errors).toEqual([
        {
          type: "header",
          row: 0,
          column: idx,
          message: `Expected header to be named "${
            headerArray[idx]
          }" but received 1.`
        }
      ]);
    });
  });
});

describe("validate - bodyLines", () => {
  it("should correctly process CSV body with expected column structure", () => {
    const contents = `Product name,Price,Quantity
    Mollis consequat,9.00,2`;
    const errors = validate(contents);
    expect(errors).toEqual([]);
  });
  it("should filter out empty rows", () => {
    const contents = `Product name,Price,Quantity

    Mollis consequat,9.00,2`;
    const errors = validate(contents);
    expect(errors).toEqual([]); // empty row should not be treated as error row
  });

  it("should return error when body row does not have sufficient columns ", () => {
    const contents = `Product name,Price,Quantity
    Mollis consequat,2`;
    const errors = validate(contents);
    expect(errors).toEqual([
      {
        type: "row",
        row: 1,
        column: -1,
        message: `Expected row to have 3 cells but received 2.`
      }
    ]);
  });
  it("should correctly process body row with extra columns ", () => {
    const contents = `Product name,Price,Quantity
    Mollis consequat,9.00,2,15`;
    const errors = validate(contents);
    expect(errors).toEqual([]);
  });
});

describe("validate - String cell", () => {
  it("should not return error for correct cell", () => {
    const contents = `Product name,Price,Quantity
    Mollis consequat,9.00,2`;
    const errors = validate(contents);
    expect(errors).toEqual([]);
  });
  it("should return error for empty cell", () => {
    const contents = `Product name,Price,Quantity
    ,9.00,2`;
    const errors = validate(contents);
    expect(errors).toEqual([
      {
        type: "cell",
        row: 1,
        column: 0,
        message: `Expected cell to be a nonempty string but received "".`
      }
    ]);
  });
  // !!! Issue in existing CartParser validate code - it would just skip a cell if unknown type is provided
});

describe("validate - Number cell", () => {
  it("should trim empty space from cells", () => {
    const contents = `Product name,Price,Quantity
    Mollis consequat,     9.00 , 2 `;
    const errors = validate(contents);
    expect(errors).toEqual([]); // all value cells are properly recognized due to trimming
  });
  it("should not return error for correct cell - real and integer values", () => {
    const contents = `Product name,Price,Quantity
    Mollis consequat,9.00,2`;
    const errors = validate(contents);
    expect(errors).toEqual([]);
  });
  it("should return error for negative cell", () => {
    const contents = `Product name,Price,Quantity
    Mollis consequat,-1,0`;
    const errors = validate(contents);
    expect(errors).toEqual([
      {
        type: "cell",
        row: 1,
        column: 1,
        message: `Expected cell to be a positive number but received "-1".`
      }
    ]);
  });
  it("should not return error for a zero-value cell", () => {
    const contents = `Product name,Price,Quantity
    Mollis consequat,0,0`;
    const errors = validate(contents);
    expect(errors).toEqual([]);
  });
  it("should not return error for empty cell", () => {
    const contents = `Product name,Price,Quantity
    Mollis consequat,,0`;
    const errors = validate(contents);
    expect(errors).toEqual([]); // empty cell is treated as 0, so no error
  });
  it("should return error for non-numeric cell", () => {
    const contents = `Product name,Price,Quantity
    Mollis consequat,Mollis,2`;
    const errors = validate(contents);
    expect(errors).toEqual([
      {
        type: "cell",
        row: 1,
        column: 1,
        message: `Expected cell to be a positive number but received "Mollis".`
      }
    ]);
  });
});

describe("validate - integral logic", () => {
  it("should capture header, row and cell errors in the correct order", () => {
    const contents = `Product name,SomeValue,Quantity
    
    Mollis consequat,9.00,2
    ,0
    Mollis 2,Mollis,-2
    `;
    const errors = validate(contents);
    expect(errors).toEqual([
      {
        type: "header",
        row: 0,
        column: 1,
        message: `Expected header to be named "Price" but received SomeValue.`
      },
      {
        type: "row",
        row: 1,
        column: -1,
        message: `Expected row to have 3 cells but received 1.`
      },
      {
        type: "row",
        row: 3,
        column: -1,
        message: `Expected row to have 3 cells but received 2.`
      },
      {
        type: "cell",
        row: 4,
        column: 1,
        message: `Expected cell to be a positive number but received "Mollis".`
      },
      {
        type: "cell",
        row: 4,
        column: 2,
        message: `Expected cell to be a positive number but received "-2".`
      },
      {
        type: "row",
        row: 5,
        column: -1,
        message: `Expected row to have 3 cells but received 1.`
      }
    ]);
  });
});

describe("parse logic", () => {
  it("should parse a correct csv file, produce correct items and total", () => {
    const pathToCsv = nodePath.join(__dirname, "/../samples/cart.csv");
    fs.readFileSync.mockReturnValue(`Product name,Price,Quantity
    Mollis consequat,9.00,2
    Tvoluptatem,10.32,1
    Scelerisque lacinia,18.90,1
    Consectetur adipiscing,28.72,10
    Condimentum aliquet,13.90,1`);
    const result = parse(pathToCsv);
    expect(parse).not.toThrow();
    expect(Array.isArray(result.items)).toBe(true);
    expect(result.items.length).toEqual(5);
    expect(result.total).toBeCloseTo(348.32, PRECISION);
    expect(result.items[0]).toEqual({
      id: "1",
      name: "Mollis consequat",
      price: 9,
      quantity: 2
    });
  });
  it("should return validation error for incorrect csv file", () => {
    const pathToErrorCsv = __dirname + "\\samples\\error_cart.csv";
    fs.readFileSync.mockReturnValue(`Product name,SomeValue,Quantity
    Mollis consequat,9.00,2
    ,0,
    Mollis 2,Mollis,-2`);
    const consoleErrorSpy = jest.spyOn(console, "error");
    expect(() => {
      parse(pathToErrorCsv);
    }).toThrowError("Validation failed!");
    expect(consoleErrorSpy).toHaveBeenCalledTimes(1);
    expect(parse).toThrowError("Validation failed!");
  });
});

describe("CartParser - integration test", () => {
  // Add your integration test here.
  const parser = new CartParser();
  const pathToCsv = __dirname + "\\samples\\cart.csv";
  const pathToJSON = __dirname + "\\samples\\sample_cart.json";
  fs.readFileSync.mockReturnValue(`Product name,Price,Quantity
  Mollis consequat,9.00,2
  Tvoluptatem,10.32,1
  Scelerisque lacinia,18.90,1
  Consectetur adipiscing,28.72,10
  Condimentum aliquet,13.90,1`);
  const result = parser.parse(pathToCsv);
  const expectedJSON = {
    items: [
      {
        id: "1",
        name: "Mollis consequat",
        price: 9,
        quantity: 2
      },
      {
        id: "1",
        name: "Tvoluptatem",
        price: 10.32,
        quantity: 1
      },
      {
        id: "1",
        name: "Scelerisque lacinia",
        price: 18.9,
        quantity: 1
      },
      {
        id: "1",
        name: "Consectetur adipiscing",
        price: 28.72,
        quantity: 10
      },
      {
        id: "1",
        name: "Condimentum aliquet",
        price: 13.9,
        quantity: 1
      }
    ],
    total: 348.32
  };
  expect(result).toEqual(expectedJSON);
});
